# The CHATONS Manifesto

## Preambule

Specters haunt our digital lives: the centralization of online services, the insecurity of our personal data, the threats to our freedom of expression and the sharing of information. 

Spied upon by multinationals and States, the trails citizens leave on the Internet and the information concerning their behaviours are used to commercial ends or to surveil the masses. Whatever may be the justifications for these uses of our data, it is imperative that we preserve spaces of private communication: spaces that will safeguard our freedoms, beginning with one of the most precious of them all, the freedom to communicate without hindrance. 

A small number of economic actors of the Internet have over the years reached monopoly positions that imply a centralization of services. This lack of diversity raises the question of the sovereignty of nations and peoples, as it results in inequalities in access to information, unacceptable censorship, mass gathering of personal data, and selective sorting of information to facilitate the invasion of advertising and marketing.

Extensions of ourselves, our personal data tell about who we are, our political and sexual orientations, our favorite topics, our dreams and objectives. As they constitute essential elements in the private lives of individuals, access to these informations must be decided by individuals themselves according to their own will and in full knowledge. The respect of privacy is today in grave danger not only in regard to protection against malevolent acts, but also from the political, legislative or economic standpoint. 

Access to information and the freedom to communicate rely on the assumption that information supports are at all time verifiable and sustainable. In that perspective, the freedoms which define free software are essential corollaries to the freedoms of computer users and citizens. Protecting those freedoms amounts to: 
  - ensuring the sustainability of information by using free and open formats;
  - decentralizing services by multiplying solutions that are accessible, reliable and based on free software;
  - allowing everyone to escape abusive legal clauses locking data into computer systems that do not respect privacy;
  - informing users of all the risks that threaten our freedoms and allowing them to overcome them.

# 1. The CHATONS, Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires

Launched in 2014, the "De-google-ify Internet" project of the French nonprofit Framasoft has demonstrated, in practice, that it was possible to gather the skills and the means required to decentralize the Internet and, by disseminating the appropriate information, help individuals and organizations do it themselves. 

This experience, other passionate communities had long since demonstrated also through various projects. They promoted both the emergence of new services based on free software and innovation in programming and engineering. Thus appeared a whole network of initiatives which offer users many opportunities to regain control of their data, preserve as much as it is possible their privacy, and even contribute in their own way to the birth of a new world, free and solidaire, an objective that has always been there in the history of the Internet and hackers since over 40 years.

It is today proposed to hosters that adhere to the principles of free software and attach fundamental importance to individual and social freedoms to participate in a collective called CHATONS for Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires. (CHATONS means KITTENS in French, and the full acronym means Collective of Alternative, Transparent, Open, Neutral and Solidaire Hosters.)

The objective of this collective is to network initiatives of free-software based online services that are offered to users along with of the useful information enabling the public to choose their services according to their needs and with a maximum amount of confidence in the hosters when it comes to their privacy, the absence of advertising and abusive or obscure legal clauses.


# 2. The Commitments

Members of the collective are committed to the respect of the charter of the collective, whose principles are the following.

## 2.1 Transparency, Non-Discrimination and Personal Data 

Probity is the key word in these commitments, according to several points aiming to ensure the reliability of the services offered and the confidence of users toward them.

The Terms of Service (TOS) must be perfectly clear, accessible and not in contradiction with the CHATONS charter.

The hoster must agree to and display an open policy regarding the administration of user accounts: no discrimination, whether the access be free of charge or not, in full compliance of all applicable laws.

The hoster commits to providing a way for all users to recover their personal data, encrypted or not, except for online services relying on the ephemeral transfer of encrypted personal data.


## 2.2 Openness, Economy, Protection

The services offered must satisfy a few technical requirements. For starters, servers must rely on free software solutions. The software must make it possible to reproduce the service without requiring additional development relative to the server structure or to the software itself.

The use of free and open formats is an obligation, minimally when it comes to all the data exposed to the users. Therefore, when the use of free and open formats is impossible (for example because it would imply downloading and installing a new piece of software on the proprietary operating system of the user), the data must instead be made available under a free content license for the maximum number of operating systems. Sources must also be made accessible.

The members of CHATONS commit to respecting the terms of the free software licenses they use (including mentioning these licenses, linking to the source code, etc.).

As a matter of ethics, sponsoring is accepted, so is patronage, donation or having an economic model that consists is asking for payment for some functionalities or even the whole service. The economic model of each member of the CHATONS must be clearly expressed on a dedicated page that the user can easily look up and understand. Obviously, the economic aspects of the activity of each member of CHATONS must be rigorously conform to the laws.

However, it will not be permitted by CHATONS members to make use of advertising coming from an advertising network. No exploitation of personal data can be accomplished, tracking the actions of users will only be made to legal and statistical ends, and the addresses of users can only be used to administrative of technical ends. The statistical tools will also need to be free-software based and satisfy the conditions of the collective.

If the hoster offers a service or functionalities in exchange of a financial contribution from the user, the fact of offering free-software based services must not be used only as a commercial argument, but as an ethical argument. Likewise, the functionality consisting in the encryption of data cannot be considered as a payable option: encryption is one of the key elements of privacy protection and the freedom to communicate: as such, it is considered as a right and not as a good to be merchandised. If the hoster has the capacity to encrypt (and therefore protect) the data of users, it must be offered as a matter of duty.

## 2.3 Solidarity and Dissemination

Members of CHATONS must help and assist one another, through a dedicated mailing list or other means at their disposal, including periodical assizes or meetings. It is how the members of CHATONS will be able to improve their services. One of the most effective means of keeping alive their mutual assistance is to contribute to the free software they use.

Members of CHATONS must however not stick to themselves and be satisfied with a limited number of users, which is likely to cause discrimination in the access to services. On the contrary, all communication efforts toward the public are encouraged as a way to disseminate free-software based solutions and to create bonds of solidarity around the core principles defended by the collective. These efforts must be mutualized and can take the form of online courses, public information meetings, booths during events, conferences, publishing booklets, etc.

## 2.4 Neutrality

The services of a member of CHATONS cannot be hosted through an actor which, by reputation, does not favour net neutrality.

Data packets must transit without discrimination in the services of a member of CHATONS, which means their contents, source and destination must not be inspected.

No communication protocol will be privileged in the distribution of information. The contents of data will not be arbitrarily altered.

Any violation of the principles of net neutrality, either by a member of the collective or by an exterior force affecting a member or the collective as a whole, will have to be reported so as to find a quick solution. If the violation must be publicly denounced, it will be so in the name of the collective only after being approved by a vote of members.

The neutrality of CHATONS is also a political neutrality in the sense that the political convictions of each member will not be examined or sanctioned so long as they do not go beyond respect of all applicable laws. 

# 3. General Policy

## 3.1 Public Relations

The collective having no official status, no one will be allowed to speak in its name without first gaining the approval of members through a majority vote. However, every member is encouraged to freely spread knowledge of the collective.

Great importance will be attached to the geographical location and the main characteristics of members of the collective on the chatons.org domain, which will be the principal directory of all members.

If needed, the collective will be permitted to speak (collectively) through press releases made available on the chatons.org domain: to welcome a new member, to state a position on a news topic, etc.

A logo will be available, if members desire so, and every member will be free to use it or not on their own web sites.

## 3.2 Structures of the Members

The members of CHATONS can be nonprofits, individuals, businesses, collectives (non exhaustive list). Essentially:

  * each member will have to appoint a unique delegate or delegation (and inform in case of replacement) who will be the main contact with other members. Either a single person or a group, so long as it is simple and obvious to establish contact (for example, using a single e-mail address).
  * each member of the collective will minimally publish a web page presenting the services accessible to the public. It is to that address that the list of members of the collective will refer.

## 3.3 Functioning of the CHATONS Collective

The CHATONS collective has a mode of governance inspired directly by the free software world. Decisions concerning the evolution of the collective and its charter will be taken in a collegial manner. As with source code, the model of the collective will be copyable and modifiable so that it can be adapted for example to regional needs.

Each member is invited to participate to the collective decisions that will be taken, as much as possible, in a consensual manner. In case of an opinion conflict, it will be possible to decide by a majority vote.

The chatons.org domain is managed and hosted by Framasoft (for as long as it will be possible and unless the collective decides otherwise). It will present a web site containing the list of members as well as a mailing list allowing exchange between members. The later will be invited to collaborate to the contents published on the site as a way to publicly communicate information relative to CHATONS and free-software based hosting.

There is no administrative level for CHATONS, which is primarily a public list of members as well as documentation intended to facilitate the sharing of knowledge, best practices and dissemination. 

Any organization respecting the principles of the present manifesto and the CHATONS charter can ask to become a member. To be maintained as a member, the organization will have to communicate to the collective the information regarding its contact person or group and subscribe at least one of its members on the mailing list. 

After discussion and eventually the sharing of a few advices, a simple majority vote will be held concerning the entry of the new organization in the collective. 

One member or more can however reserve the right to ask for the exclusion of another member at the following conditions:

  * to detail the proposition with compelling evidence shared with all members;
  * to accept a collective vote, without or without contradictory debate.

Aware that it is not possible to guaranty the full respect of the CHATONS charter without undermining the confidentiality of personal data hosted on the computer systems of members, the control by peers will *de facto* be imperfect. The collective therefore relies first and foremost upon the trust and the goodwill shared by members.

The CHATONS must therefore find, among themselves, and in the respect of the point of view of each member, the good practices and the rules for managing the inclusion, questioning or exclusion of members of the collective, that will be in the best interest of the fundamental freedoms and privacy of the users of the services offered by the collective.